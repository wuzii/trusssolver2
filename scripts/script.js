// $.getScript("fabricLineArrow.js", function () {
//     // alert("Script loaded.");
// });

const toolMap = {
    select: { img: "trusssolver2/images/pointer.svg", cursor_img: "" },
    member: { img: "trusssolver2/images/member.svg", cursor_img: "" },
    load: { img: "trusssolver2/images/load.svg", cursor_img: "trusssolver2/images/load.svg" },
    pin: { img: "trusssolver2/images/pin.svg", cursor_img: "trusssolver2/images/pin.svg" },
    roller: { img: "trusssolver2/images/roller.svg", cursor_img: "trusssolver2/images/roller.svg" }
};

const jointAttrNormal = {
    strokeWidth: 5,
    radius: 9,
    fill: '#fff',
    stroke: '#555'
};

const jointAttrSelected = {
    strokeWidth: 6,
    radius: 12,
    fill: '#fff',
    stroke: 'red'
};

const jointAttrClickable = {
    strokeWidth: 6,
    radius: 16,
    fill: '#ddd',
    stroke: '#555'
};

const jointAttrClickableHover = {
    strokeWidth: 6,
    radius: 16,
    fill: '#ffcdd2',
    stroke: '#555'
};

const loadAttrNormal = {
    fill: '#4caf50',
    stroke: '#4caf50',
    strokeWidth: 5,
    strokeLineCap: 'round'
}

const loadAttrSelected = {
    fill: 'red',
    stroke: 'red',
    strokeWidth: 5,
    strokeLineCap: 'round'
}

const memberAttrNormal = {
    fill: '#2196f3',
    stroke: '#2196f3',
    strokeWidth: 6,
    strokeLineCap: 'round'
};

const memberAttrSelected = {
    fill: 'red',
    stroke: 'red',
    strokeWidth: 6,
    strokeLineCap: 'round'
};

const rollerAttrNormal = pinAttrNormal = {
    opacity: 1,
    filters: [new fabric.Image.filters.Grayscale()]
};

const rollerAttrSelected = pinAttrSelected = {
    opacity: 1,
    filters: []
};

const labelAttr = {
    fontSize: 23,
    fontWeight: 'bold',
    fontFamily: 'sans-serif',
    selectable: false,
    evented: false
}

const tempMemberAttr = {
    fill: 'grey',
    stroke: 'grey',
    strokeWidth: 3,
    strokeDashArray: [4, 2],
    selectable: false,
    evented: false,
    opacity: 0.7
};

const tempLoadAttr = {
    fill: 'grey',
    stroke: 'grey',
    strokeWidth: 3,
    strokeDashArray: [4, 2],
    selectable: false,
    evented: false,
    opacity: 0.7
};

const loadArrowLengthPixel = 100;

htmlCanvas = document.getElementById('c');
htmlCanvas.width = window.innerWidth;
htmlCanvas.height = window.innerHeight;
var canvas = this.__canvas = new fabric.Canvas('c', { selection: false });
fabric.Object.prototype.originX = fabric.Object.prototype.originY = 'center';

var touch_device = false;
var realScreenWidth = 1000;
var scale = 1;  // = real length/unzoomed length on canvas
var loadRealLength = 1;
var arrMembers = new Array();
var arrJoints = new Array();
var arrLoads = new Array();
var arrSupports = new Array();
var arrLabels = new Array();

var toolState = {
    cur_tool: "select",
    cursor_obj: null,
    selected_obj: null,
    member_drawing: false,
    member_temp_joint: null,
    member_temp_line: null,
    load_drawing: false,
    load_temp_joint: null,
    load_temp_arrow: null
};

function clearAll() {
    arrMembers = new Array();
    arrJoints = new Array();
    arrLoads = new Array();
    arrSupports = new Array();
    arrLabels = new Array();
    toolState.selected_obj = null;
    toolState.member_drawing = false;
    toolState.member_temp_joint = null;
    toolState.member_temp_line = null;
    toolState.load_drawing = false;
    toolState.load_temp_joint = null;
    toolState.load_temp_arrow = null;
    canvas.clear();
    canvas.renderAll();
    activateTool(toolState.cur_tool);
}

function initScale(screenWidthReal) {
    realScreenWidth = screenWidthReal;
    scale = realScreenWidth / canvas.getWidth();
    loadRealLength = loadArrowLengthPixel * scale;
    updateScaleText();
}

function updateScaleText() {
    var scaleRealLength = ($("#scale").outerWidth() * scale) / canvas.getZoom();
    $("#scale p").text(scaleRealLength.toPrecision(3));
}

// unzoomed coor to real length, angle (-180 to +180 degrees from +x axis)
function coorToLengthAngle(x1, y1, x2, y2) {
    var dx = scale * (x2 - x1);
    var dy = -1 * scale * (y2 - y1);    // y coor start from top
    return {
        length: Math.sqrt(dx * dx + dy * dy),
        angle: (180 / Math.PI) * Math.atan2(dy, dx)
    };
}

function toEndPoint(x1, y1, realLength, angle) {
    x2 = x1 + (realLength * Math.cos(angle / 180 * Math.PI) / scale);
    y2 = y1 - (realLength * Math.sin(angle / 180 * Math.PI) / scale);
    return { x2: x2, y2: y2 };
}


// Member is a fabric.Line with 2 joint attributes
function makeMemberFromJoints(joint1, joint2) {
    // check member exist
    if ((!joint1.neighborJoints.includes(joint2)) && (joint1 != joint2)) {
        var m = new fabric.Line(
            [joint1.get("left"), joint1.get("top"),
            joint2.get("left"), joint2.get("top")],
            {
                selectable: true,
                evented: true,
                perPixelTargetFind: true,
                lockMovementX: true,
                lockMovementY: true,
                lockRotation: true,
                lockScalingX: true,
                lockScalingY: true,
                hoverCursor: "pointer"
            });
        m.set(memberAttrNormal);
        m.hasControls = false;
        m.hasBorders = false;

        m.joint1 = joint1;
        m.joint2 = joint2;
        joint1.members.push(m);
        joint1.neighborJoints.push(joint2);
        joint2.members.push(m);
        joint2.neighborJoints.push(joint1);
        canvas.add(m);
        m.sendToBack();
        arrMembers.push(m);
        return m;
    } else {
        return null;
    }
}

// Joint is a fabric.Circle with a members attribute that stores an array of Member
// and a neighborJoints attr that stores all joints that members connect to
function makeJointFromCoor(x, y) {
    var c = new fabric.Circle({
        left: x, top: y,
        lockMovementX: true,
        lockMovementY: true,
        lockRotation: true,
        lockScalingX: true,
        lockScalingY: true,
        hoverCursor: "pointer"
    });
    c.set(jointAttrNormal);
    c.hasControls = false;
    c.hasBorders = false;

    c.neighborJoints = new Array();
    c.members = new Array();
    c.load = null;
    c.support = null;
    c.labelLetter = "";
    canvas.add(c);
    c.bringToFront();
    arrJoints.push(c);
    return c;
}

function makeRollerFromJoint(joint) {
    var imgElement = document.getElementById('roller-img');
    var c = new fabric.Image(imgElement, {
        left: joint.get("left"),
        selectable: true,
        evented: true,
        perPixelTargetFind: true,
        lockMovementX: true,
        lockMovementY: true,
        lockRotation: true,
        lockScalingX: true,
        lockScalingY: true,
        hoverCursor: "pointer"
    });
    c.set({ top: joint.get("top") + (c.height / 2) });
    c.set(rollerAttrNormal);
    c.applyFilters();
    c.hasControls = false;
    c.hasBorders = false;

    c.support_type = "roller";
    c.joint = joint;
    joint.support = c;
    canvas.add(c);
    c.bringToFront();
    arrSupports.push(c);
    return c;
}

function makePinFromJoint(joint) {
    var imgElement = document.getElementById('pin-img');
    var c = new fabric.Image(imgElement, {
        left: joint.get("left"),
        selectable: true,
        evented: true,
        perPixelTargetFind: true,
        lockMovementX: true,
        lockMovementY: true,
        lockRotation: true,
        lockScalingX: true,
        lockScalingY: true,
        hoverCursor: "pointer"
    });
    c.set({ top: joint.get("top") + (c.height / 2) });
    c.set(pinAttrNormal);
    c.applyFilters();
    c.hasControls = false;
    c.hasBorders = false;

    c.support_type = "pin";
    c.joint = joint;
    joint.support = c;
    canvas.add(c);
    arrSupports.push(c);
    return c;
}


function makeLoadFromJointCoor(joint, x2, y2, magnitute) {
    var c = new fabric.LineArrow(
        [joint.get("left"), joint.get("top"),
            x2, y2],
        {
            selectable: true,
            evented: true,
            perPixelTargetFind: true,
            lockMovementX: true,
            lockMovementY: true,
            lockRotation: true,
            lockScalingX: true,
            lockScalingY: true,
            hoverCursor: "pointer"
        });
    c.set(loadAttrNormal);
    c.hasControls = false;
    c.hasBorders = false;

    c.magnitute = magnitute;
    c.loadAngle = coorToLengthAngle(joint.get("left"), joint.get("top"), x2, y2).angle;
    c.joint = joint;
    joint.load = c;
    canvas.add(c);
    c.sendToBack();
    arrLoads.push(c);
    return c;
}

function makeLoadFromJoint(joint, angle, magnitute) {
    var endPoint = toEndPoint(joint.get("left"), joint.get("top"), loadRealLength, angle);
    return makeLoadFromJointCoor(joint, endPoint.x2, endPoint.y2, magnitute);
}

function resetMemberToolState() {
    unSelect();
    if (toolState.member_drawing == true) {
        toolState.member_drawing = false;
        disableInput("member");
        canvas.remove(toolState.member_temp_line);
        if (toolState.member_temp_joint && !arrJoints.includes(toolState.member_temp_joint)) {
            canvas.remove(toolState.member_temp_joint); // remove if just a temp
        }
        toolState.member_temp_line = null;
        toolState.member_temp_joint = null;
    }
}

function resetLoadToolState() {
    unSelect();
    if (toolState.load_drawing == true) {
        toolState.load_drawing = false;
        disableInput("load");
        canvas.remove(toolState.load_temp_arrow);
        toolState.load_temp_arrow = null;
        toolState.load_temp_joint = null;
    }
}


function disableJoints() {
    for (var i = 0; i < arrJoints.length; i++) {
        arrJoints[i].set(jointAttrNormal);
        arrJoints[i].off('mousemove');
        arrJoints[i].off('mouseover');
        arrJoints[i].off('mouseout');
    }
    canvas.renderAll();
}

function clickableJoints() {
    for (var i = 0; i < arrJoints.length; i++) {
        arrJoints[i].set(jointAttrClickable);

        if (!touch_device) {
            arrJoints[i].on('mouseover', function (e) {
                e.target.set(jointAttrClickableHover);
                canvas.renderAll();
            });

            arrJoints[i].on('mouseout', function (e) {
                if (e.target) {
                    e.target.set(jointAttrClickable);
                }
                canvas.renderAll();
            });

            arrJoints[i].on('mousemove', function (e) {
                if (toolState.member_drawing == true) {
                    e.target.set(jointAttrClickableHover);
                    toolState.member_temp_line.set({ x2: e.target.left, y2: e.target.top });
                    canvas.renderAll();
                }
            });
        }
    }
    canvas.renderAll();
}

function unSelect() {
    activateInfobar(toolState.cur_tool);
    if (toolState.selected_obj) {
        if (arrJoints.includes(toolState.selected_obj)) {
            toolState.selected_obj.set(jointAttrNormal);
        } else if (arrMembers.includes(toolState.selected_obj)) {
            toolState.selected_obj.set(memberAttrNormal);
        } else if (arrLoads.includes(toolState.selected_obj)) {
            toolState.selected_obj.set(loadAttrNormal);
        } else if (arrSupports.includes(toolState.selected_obj)) {
            if (toolState.selected_obj.support_type == "roller") {
                toolState.selected_obj.set(rollerAttrNormal);
                toolState.selected_obj.applyFilters();
            } else if (toolState.selected_obj.support_type == "pin") {
                toolState.selected_obj.set(pinAttrNormal);
                toolState.selected_obj.applyFilters();
            }
        }
        canvas.renderAll();
        toolState.selected_obj = null;
    }
}

function setSelected(obj) {
    if (obj) {
        if (toolState.selected_obj) {
            unSelect();
        }
        toolState.selected_obj = obj;
        if (arrJoints.includes(obj)) {
            obj.set(jointAttrSelected);
        } else if (arrMembers.includes(obj)) {
            obj.set(memberAttrSelected);
            activateInfobar("member");
            updateMemberForm();
        } else if (arrLoads.includes(obj)) {
            obj.set(loadAttrSelected);
            activateInfobar("load");
            updateLoadForm();
        } else if (arrSupports.includes(obj)) {
            if (obj.support_type == "roller") {
                obj.set(rollerAttrSelected);
                obj.applyFilters();
            } else if (obj.support_type == "pin") {
                obj.set(pinAttrSelected);
                obj.applyFilters();
            }
        }
        canvas.renderAll();
    }
}



function activateTool(toolName) {
    $("#toolbar .tool.active img").attr("src", toolMap[toolName]["img"]);
    $("#toolbar .tool.active").attr("tool-name", toolName);
    toolState.cur_tool = toolName;
    initCursor(toolState.cur_tool);
    disableJoints();
    resetMemberToolState();
    resetLoadToolState();
    activateInfobar(toolState.cur_tool);

    if (toolName == "member") {
        clickableJoints();
    } else if (toolName == "load") {
        clickableJoints();
    } else if (toolName == "roller") {
        clickableJoints();
    } else if (toolName == "pin") {
        clickableJoints();
    }
}

function activateInfobar(toolName) {
    $("#infobar > form").removeClass("active");
    $("#infobar > form[tool-name='" + toolName + "']").addClass("active");
}

function enableInput(toolName) {
    $("#infobar > form[tool-name='" + toolName + "'] input").removeAttr("disabled");
    $("#infobar > form[tool-name='" + toolName + "'] button").removeAttr("disabled");
}

function disableInput(toolName) {
    $("#infobar > form[tool-name='" + toolName + "'] input").attr("disabled", true);
    $("#infobar > form[tool-name='" + toolName + "'] button").attr("disabled", true);
}


function initCursor(toolName) {
    if (toolState.cursor_obj) {
        canvas.remove(toolState.cursor_obj);
        toolState.cursor_obj = null;
    }
    if (!touch_device) {
        $("#cursor-img").attr("src", toolMap[toolName].cursor_img);
        var imgElement = document.getElementById('cursor-img');

        if (toolName == "roller") {
            toolState.cursor_obj = new fabric.Image(imgElement, {
                left: 0,
                top: 0,
                opacity: 0.0,
                selectable: false,
                evented: false,
            });
            canvas.add(toolState.cursor_obj);
        } else if (toolName == "pin") {
            toolState.cursor_obj = new fabric.Image(imgElement, {
                left: 0,
                top: 0,
                opacity: 0.0,
                selectable: false,
                evented: false,
            });
            canvas.add(toolState.cursor_obj);
        } else if (toolName == "load") {
            toolState.cursor_obj = new fabric.Image(imgElement, {
                left: 0,
                top: 0,
                opacity: 0.0,
                selectable: false,
                evented: false,
            });
            canvas.add(toolState.cursor_obj);
        }
    }
}

function handleMemberMouseDown(e) {
    var unzoomedPointer = e.unzoomedPointer;
    removeLabel();
    if (toolState.member_drawing == false) {
        if (e.target && e.target.type == "circle" && arrJoints.includes(e.target)) {
            // start joint is one of the existing joints
            toolState.member_temp_joint = e.target;
            e.target.set(jointAttrClickableHover);
        } else {
            // create a temp joint
            toolState.member_temp_joint = new fabric.Circle({ left: unzoomedPointer.x, top: unzoomedPointer.y });
            toolState.member_temp_joint.set(jointAttrClickableHover);
            toolState.member_temp_joint.hasControls = false;
            toolState.member_temp_joint.hasBorders = false;
            canvas.add(toolState.member_temp_joint)
        }
        toolState.member_drawing = true;
        enableInput("member");
        toolState.member_temp_line = new fabric.Line(
            [toolState.member_temp_joint.get("left"), toolState.member_temp_joint.get("top"),
            unzoomedPointer.x, unzoomedPointer.y], tempMemberAttr);
        canvas.add(toolState.member_temp_line);
    } else {
        var joint1, joint2;
        if (arrJoints.includes(toolState.member_temp_joint)) {
            // start joint is one of the existing joints
            joint1 = toolState.member_temp_joint;
        } else {
            // start joint is a temp, remove it and create a real one
            canvas.remove(toolState.member_temp_joint);
            joint1 = makeJointFromCoor(toolState.member_temp_joint.get('left'),
                toolState.member_temp_joint.get('top'));
        }
        if (e.target && e.target.type == "circle" && arrJoints.includes(e.target)) {
            // end joint is one of the existing joints
            joint2 = e.target;
            if (joint2.neighborJoints.includes(joint1)) {
                // if making a member that's already on the graph
                resetMemberToolState();
                return;
            }
        } else {
            joint2 = makeJointFromCoor(unzoomedPointer.x, unzoomedPointer.y);
        }
        var member = makeMemberFromJoints(joint1, joint2);
        resetMemberToolState();
        clickableJoints();  // make new joints clickable            
    }
}

function handleLoadMouseDown(e) {
    var unzoomedPointer = e.unzoomedPointer;

    if (toolState.load_drawing == false) {
        if (e.target && e.target.type == "circle" && arrJoints.includes(e.target)) {
            removeLabel();
            var joint = e.target;
            if (!joint.load) {
                var lengthAngle = coorToLengthAngle(joint.get("left"), joint.get("top"), unzoomedPointer.x, unzoomedPointer.y);
                var endPoint = toEndPoint(joint.get("left"), joint.get("top"), loadRealLength, lengthAngle.angle);

                toolState.load_temp_arrow = new fabric.LineArrow(
                    [joint.get("left"), joint.get("top"),
                    endPoint.x2, endPoint.y2],
                    tempLoadAttr
                );
                toolState.load_temp_arrow.hasControls = false;
                toolState.load_temp_arrow.hasBorders = false;
                toolState.load_temp_joint = joint;
                toolState.load_drawing = true;
                enableInput("load");
                canvas.add(toolState.load_temp_arrow);
            }
        }
    } else {
        removeLabel();
        var lengthAngle = coorToLengthAngle(toolState.load_temp_joint.get("left"),
            toolState.load_temp_joint.get("top"),
            unzoomedPointer.x, unzoomedPointer.y);
        var load = makeLoadFromJoint(toolState.load_temp_joint, lengthAngle.angle, $("#loadMag").val());
        resetLoadToolState();
    }
}

function handleSupportMouseDown(e, type) {
    if (e.target && arrJoints.includes(e.target) && !e.target.support) {
        removeLabel();

        if (type == "roller") {
            makeRollerFromJoint(e.target);
        } else if (type == "pin") {
            makePinFromJoint(e.target);
        }
    }
}

function updateMemberForm() {
    if (toolState.member_drawing) {
        var lengthAngle = coorToLengthAngle(
            toolState.member_temp_line.get("x1"),
            toolState.member_temp_line.get("y1"),
            toolState.member_temp_line.get("x2"),
            toolState.member_temp_line.get("y2"));

        $("#memberLength").val(lengthAngle.length);
        $("#memberAngle").val(lengthAngle.angle);
    } else if (toolState.cur_tool == "select" && arrMembers.includes(toolState.selected_obj)) {
        var lengthAngle = coorToLengthAngle(
            toolState.selected_obj.get("x1"),
            toolState.selected_obj.get("y1"),
            toolState.selected_obj.get("x2"),
            toolState.selected_obj.get("y2"));
        $("#memberLength").val(lengthAngle.length);
        $("#memberAngle").val(lengthAngle.angle);
    }
}

function updateLoadForm() {
    if (toolState.load_drawing) {
        var lengthAngle = coorToLengthAngle(
            toolState.load_temp_arrow.get("x1"),
            toolState.load_temp_arrow.get("y1"),
            toolState.load_temp_arrow.get("x2"),
            toolState.load_temp_arrow.get("y2"));

        // $("#loadMag").val(10);
        $("#loadAngle").val(lengthAngle.angle);
    } else if (toolState.cur_tool == "select" && arrLoads.includes(toolState.selected_obj)) {
        $("#loadMag").val(toolState.selected_obj.magnitute);
        $("#loadAngle").val(toolState.selected_obj.loadAngle);
    }
}

function deleteFromArray(arr, obj) {
    arr.splice(arr.indexOf(obj), 1);
}

function deleteLoad(load) {
    load.joint.load = null;
    deleteFromArray(arrLoads, load);
    canvas.remove(load);
}

function deleteMember(member) {
    if (member.joint1.members.length == 1) {
        if (member.joint1.load) { deleteLoad(member.joint1.load); }
        if (member.joint1.support) { deleteSupport(member.joint1.support); }
        deleteFromArray(arrJoints, member.joint1);
        canvas.remove(member.joint1);
    } else {
        deleteFromArray(member.joint1.members, member);
        deleteFromArray(member.joint1.neighborJoints, member.joint2);
    }

    if (member.joint2.members.length == 1) {
        if (member.joint2.load) { deleteLoad(member.joint2.load); }
        if (member.joint2.support) { deleteSupport(member.joint2.support); }
        deleteFromArray(arrJoints, member.joint2);
        canvas.remove(member.joint2);
    } else {
        deleteFromArray(member.joint2.members, member);
        deleteFromArray(member.joint2.neighborJoints, member.joint1);
    }
    deleteFromArray(arrMembers, member);
    canvas.remove(member);
}

function deleteJoint(joint) {
    var numMembers = joint.members.length;
    for (var i = 0; i < numMembers; i++) {
        deleteMember(joint.members[0]);
    }
}

function deleteSupport(support) {
    support.joint.support = null;
    deleteFromArray(arrSupports, support);
    canvas.remove(support);
}

function onDeleteClicked() {
    if (toolState.cur_tool == "select") {
        if (toolState.selected_obj) {
            obj = toolState.selected_obj;
            removeLabel();
            if (arrMembers.includes(obj)) {
                deleteMember(obj);
            } else if (arrJoints.includes(obj)) {
                deleteJoint(obj);
            } else if (arrLoads.includes(obj)) {
                deleteLoad(obj);
            } else if (arrSupports.includes(obj)) {
                deleteSupport(obj);
            }
        }
        canvas.renderAll();
    }
}

function assignLabel() {
    for (var i = 0; i < arrJoints.length; i++) {
        arrJoints[i].labelLetter = "";
        for (var j = 0; j < (i + 1) / 26; j++) {
            arrJoints[i].labelLetter = arrJoints[i].labelLetter + String.fromCharCode(65 + i);
        }
    }
}

function displayLabel() {
    for (var i = 0; i < arrJoints.length; i++) {
        var text = new fabric.Text(arrJoints[i].labelLetter,
            {});
        text.set(labelAttr);
        text.left = arrJoints[i].left - (arrJoints[i].width / 2) - (text.width / 2);
        text.top = arrJoints[i].top - (arrJoints[i].height / 2) - (text.height / 2);
        text.hasControls = false;
        text.hasBorders = false;
        text.joint = arrJoints[i];
        arrLabels.push(text);
        canvas.add(text);
    }
}

function removeLabel() {
    for (var i = 0; i < arrLabels.length; i++) {
        canvas.remove(arrLabels[i]);
    }
    arrLabels = [];
}

function customAlert(title, line1, line2) {
    $("#alert-modal-label").html(title);
    $("#alert-modal .modal-body").html("<p>" + line1 + "</p><p>" + line2 + "</p>");
    $("#alert-modal").modal('show');
}

function solve() {
    try {
        if (arrMembers.length == 0) {
            customAlert("oops!", "Please add members using the <b>Member</b> tool:", "<img src='trusssolver2/images/member.svg'>");
        }
        else if (arrLoads.length == 0) {
            customAlert("oops!", "Please set the loads of the structure using the <b>Load</b> tool:", "<img src='trusssolver2/images/load.svg'>");
        }
        else if (arrSupports.length == 0) {
            customAlert("oops!", "Please add supports using the <b>Pin Support</b> or <b>Roller Support</b> tool:", "<img src='trusssolver2/images/pin.svg'><img src='trusssolver2/images/roller.svg'>");
        }
        else {
            var numMember = arrMembers.length;
            var numLoad = arrLoads.length;
            var numSupport = arrSupports.length;
            var numPin = arrSupports.reduce(function (tot, s) { return tot + (s.support_type == "pin"); }, 0);
            var numRoller = arrSupports.reduce(function (tot, s) { return tot + (s.support_type == "roller"); }, 0);

            var requestStr = "";
            requestStr = requestStr + "n_m=" + numMember + "&";
            requestStr = requestStr + "n_l=" + numLoad + "&";
            requestStr = requestStr + "n_ps=" + numPin + "&";
            requestStr = requestStr + "n_rs=" + numRoller + "&";
            var i = 0;
            for (i = 0; i < numMember; i++) {
                requestStr = requestStr + "m" + i + "x1=" + arrMembers[i].get("x1") + "&";		//m0x1.....
                requestStr = requestStr + "m" + i + "y1=" + arrMembers[i].get("y1") + "&";
                requestStr = requestStr + "m" + i + "x2=" + arrMembers[i].get("x2") + "&";
                requestStr = requestStr + "m" + i + "y2=" + arrMembers[i].get("y2") + "&";
            }
            var rollerI = -1;
            var pinI = -1;
            for (i = 0; i < numSupport; i++) {
                var supportTypeStr = "";
                var supportI = 0;
                if (arrSupports[i].support_type == "roller") {
                    supportTypeStr = "rs";
                    rollerI = rollerI + 1;
                    supportI = rollerI;
                } else if (arrSupports[i].support_type == "pin") {
                    supportTypeStr = "ps";
                    pinI = pinI + 1;
                    supportI = pinI;
                }
                requestStr = requestStr + supportTypeStr + supportI + "x=" + arrSupports[i].joint.left + "&";		//ps0x......
                requestStr = requestStr + supportTypeStr + supportI + "y=" + arrSupports[i].joint.top + "&";
            }
            for (i = 0; i < numLoad; i++) {
                requestStr = requestStr + "l" + i + "x=" + arrLoads[i].joint.left + "&";		//l0x......
                requestStr = requestStr + "l" + i + "y=" + arrLoads[i].joint.top + "&";
                requestStr = requestStr + "l" + i + "a=" + arrLoads[i].loadAngle + "&";	//l0a....
                requestStr = requestStr + "l" + i + "m=" + arrLoads[i].magnitute + "&";		//l0m....
            }
            requestStr = requestStr + "end=1";
            //alert(requestStr);
            var xmlhttp = new XMLHttpRequest();

            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var response = xmlhttp.responseText;
                    // var response = "4.20227952866&1_6.13400762&-1_6.15154705606&1_4.89584699105&1_5.78676713911&-1_1.35463098383&-1_5.75478501911&1S2.56901156343&4.43107719972_0&5.23329963415";
                    if (response.indexOf("Error") == -1) {
                        var arr_str = response.split("S");
                        var arr_str_m = arr_str[0].split("_");
                        var arr_str_s = arr_str[1].split("_");
                        var i;
                        var arr_m = new Array();
                        var arr_s = new Array();

                        var ii = arr_str_m.length;
                        for (i = 0; i < ii; i++) {
                            arr_m[i] = new Array();
                            arr_m[i][0] = arr_str_m[i].split("&")[0];
                            arr_m[i][1] = arr_str_m[i].split("&")[1];

                        }
                        ii = arr_str_s.length;
                        for (i = 0; i < ii; i++) {
                            arr_s[i] = new Array();
                            arr_s[i][0] = arr_str_s[i].split("&")[0];
                            arr_s[i][1] = arr_str_s[i].split("&")[1];
                        }

                        assignLabel();
                        var display_html = '<table id="member" class="table table-sm table-bordered"><tr><th colspan="3">Member</th></tr>';

                        for (i = 0; i < arrMembers.length; i++) {
                            var letter_1 = arrMembers[i].joint1.labelLetter;
                            var letter_2 = arrMembers[i].joint2.labelLetter;
                            display_html = display_html + '<tr><td class="id">' + letter_1 + ' ' + letter_2 + '</td>' + '<td class="value">' + arr_m[i][0] + '</td>';
                            var type;
                            if (arr_m[i][1] == 1) {
                                type = "C";
                            }
                            else {
                                type = "T";
                            }
                            display_html = display_html + '<td class="type">' + type + '</td></tr>';
                        }
                        display_html = display_html + '</table><table id="support" class="table table-sm table-bordered"><tr><th colspan="3">Support</th></tr>';
                        for (i = 0; i < arrSupports.length; i++) {
                            var letter = arrSupports[i].joint.labelLetter;
                            display_html = display_html + '<tr><td rowspan="2" class="id">' + letter + '</td><td class="type">V</td>' + '<td class="value">' + arr_s[i][1] + '</td></tr><tr><td class="type">H</td><td class="value">' + arr_s[i][0] + '</td></tr>';
                        }
                        display_html = display_html + '</table>';
                        removeLabel();
                        displayLabel();
                        $("#result-btn").attr("disabled", false);
                        $("#result-btn").attr("data-content", display_html);
                    }
                    else {
                        customAlert("oops!", "This structure is statically indeterminate.", "Please review for missing or misplaced parts");
                    }
                } else {
                    // customAlert("Connection Error", "404-Please check your internet connection and try again.", "");
                }
            }
            xmlhttp.open("POST", "/trusssolver/solve", true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(requestStr);
        }
    }
    catch (err) {
        customAlert("Error", "An error has occured while solving the structure. Click 'Ok' to continue.", "Error Message: " + err.message);
    }
}

function saveToJson() {
    jointsList = arrJoints.map(function (curVal) {
        return [curVal.left, curVal.top];
    });
    memberList = arrMembers.map(function (curVal) {
        return [arrJoints.indexOf(curVal.joint1), arrJoints.indexOf(curVal.joint2)];
    });
    loadList = arrLoads.map(function (curVal) {
        return [arrJoints.indexOf(curVal.joint), curVal.loadAngle, curVal.magnitute];
    });
    supportList = arrSupports.map(function (curVal) {
        return [arrJoints.indexOf(curVal.joint), curVal.support_type.charAt(0)];
    });
    return JSON.stringify({
        width: realScreenWidth,
        joints: jointsList,
        members: memberList,
        loads: loadList,
        supports: supportList
    });
}

function loadFromJson(jsonStr) {
    try {
        var obj = JSON.parse(jsonStr);
        clearAll();
        initScale(obj.width);
        for (var i = 0; i < obj.joints.length; i++) {
            makeJointFromCoor(obj.joints[i][0], obj.joints[i][1]);
        }
        for (var i = 0; i < obj.members.length; i++) {
            makeMemberFromJoints(arrJoints[obj.members[i][0]], arrJoints[obj.members[i][1]]);
        }
        for (var i = 0; i < obj.loads.length; i++) {
            makeLoadFromJoint(arrJoints[obj.loads[i][0]], obj.loads[i][1], obj.loads[i][2]);
        }
        for (var i = 0; i < obj.supports.length; i++) {
            if (obj.supports[i][1] == "r") {
                makeRollerFromJoint(arrJoints[obj.supports[i][0]]);
            } else if (obj.supports[i][1] == "p") {
                makePinFromJoint(arrJoints[obj.supports[i][0]]);
            }
        }
    } catch (err) {
        return false;
    }
    return true;
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? false : decodeURIComponent(sParameterName[1]);
        }
    }
    return false;
}

function genUrl() {
    var pageUrl = [location.protocol, '//', location.host, location.pathname].join('');
    return pageUrl + "?d=" + encodeURIComponent(LZString.compressToEncodedURIComponent(saveToJson()))
}

function loadFromUrl() {
    clearAll();
    var obj = LZString.decompressFromEncodedURIComponent(decodeURIComponent(getUrlParameter("d")));
    return loadFromJson(obj);
}



canvas.on('mouse:move', function (e) {
    var unzoomedPointer = this.restorePointerVpt(e.pointer);
    if (toolState.cursor_obj) {
        toolState.cursor_obj.set({
            'opacity': 0.8,
            'left': unzoomedPointer.x,
            'top': unzoomedPointer.y + toolState.cursor_obj.height / 2
        });
    }

    if (toolState.member_drawing) {
        toolState.member_temp_line.set({ 'x2': unzoomedPointer.x, 'y2': unzoomedPointer.y });
        updateMemberForm();
    } else if (toolState.load_drawing) {
        var lengthAngle = coorToLengthAngle(toolState.load_temp_joint.get("left"),
            toolState.load_temp_joint.get("top"),
            unzoomedPointer.x, unzoomedPointer.y);
        var endPoint = toEndPoint(toolState.load_temp_joint.get("left"),
            toolState.load_temp_joint.get("top"), loadRealLength, lengthAngle.angle);
        toolState.load_temp_arrow.set({ 'x2': endPoint.x2, 'y2': endPoint.y2 });
        updateLoadForm();
    }

    canvas.renderAll();
});


canvas.on('mouse:up', function (e) {
    $("#toolbar").removeClass("open");
    e.unzoomedPointer = this.restorePointerVpt(e.pointer);

    if (toolState.cur_tool == "member") {
        handleMemberMouseDown(e);
    } else if (toolState.cur_tool == "load") {
        handleLoadMouseDown(e);
    }
    else if (toolState.cur_tool == "select") {
        if (e.target) {
            setSelected(e.target);
        } else {
            unSelect();
        }
    }
    else if (toolState.cur_tool == "roller") {
        handleSupportMouseDown(e, "roller");
    }
    else if (toolState.cur_tool == "pin") {
        handleSupportMouseDown(e, "pin");
    }

    canvas.renderAll();
});


// For zooming
canvas.on('mouse:wheel', function (opt) {
    var delta = opt.e.deltaY;
    var pointer = canvas.getPointer(opt.e);
    var zoom = canvas.getZoom();
    zoom = zoom + delta / 500;
    if (zoom > 10) zoom = 10;
    if (zoom < 0.1) zoom = 0.1;
    canvas.zoomToPoint({ x: opt.e.offsetX, y: opt.e.offsetY }, zoom);
    updateScaleText();
    opt.e.preventDefault();
    opt.e.stopPropagation();
});


canvas.on("touch:gesture", function (e) {
    if (e.self.gesture == "gesture" && e.self.fingers == 2) {
        var zoom = e.self.scale;
        if (zoom > 10) zoom = 10;
        if (zoom < 0.1) zoom = 0.1;
        canvas.zoomToPoint({ x: e.self.x, y: e.self.y }, zoom);
        updateScaleText();
        e.e.preventDefault();
        e.e.stopPropagation();
    }
});

canvas.on("touch:drag", function (e) {
    if (e.self.gesture == "drag" && e.self.fingers == 2 && e.self.state == "move") {
        var deltaX = e.self.x - e.self.start.x;
        var deltaY = e.self.y - e.self.start.y;

        canvas.absolutePan({ x: -1 * deltaX, y: -1 * deltaY });
        //updateScaleText();
        e.e.preventDefault();
        e.e.stopPropagation();
    }
});


$("#toolbar .tool.active").click(function () {
    $("#toolbar").toggleClass("open");
});

$("#toolbar .tool:not(.active)").click(function () {
    $("#toolbar").removeClass("open");
    activateTool($(this).attr("tool-name"));
});

$("#toolbar .tool.active").hover(function () {
    $("#toolbar").addClass("open");
});


$("#delete-tool").click(function () {
    onDeleteClicked();
});


// click "set"
$("#infobar > form > button").click(function () {
    if (toolState.cur_tool == "member" && toolState.member_drawing == true) {
        var length = $("#memberLength").val();
        var angle = $("#memberAngle").val();
        var endPoint = toEndPoint(toolState.member_temp_joint.get('left'),
            toolState.member_temp_joint.get('top'),
            length, angle);

        var joint1, joint2;
        if (arrJoints.includes(toolState.member_temp_joint)) {
            // start joint is one of the existing joints
            joint1 = toolState.member_temp_joint;
        } else {
            // start joint is a temp, remove it and create a real one
            canvas.remove(toolState.member_temp_joint);
            joint1 = makeJointFromCoor(toolState.member_temp_joint.get('left'),
                toolState.member_temp_joint.get('top'));
        }
        joint2 = makeJointFromCoor(endPoint.x2, endPoint.y2);
        var member = makeMemberFromJoints(joint1, joint2);
        resetMemberToolState();
        clickableJoints();  // make new joints clickable         
    } else if (toolState.cur_tool == "load" && toolState.load_drawing == true) {
        var magnitute = $("#loadMag").val();
        var angle = $("#loadAngle").val();
        var load = makeLoadFromJoint(toolState.load_temp_joint, angle, magnitute);
        resetLoadToolState();
    }
});

// type in infobar
$("#infobar > form input").on('input', function () {
    if (toolState.cur_tool == "member" && toolState.member_drawing == true) {
        var endPoint = toEndPoint(
            toolState.member_temp_line.get("x1"),
            toolState.member_temp_line.get("y1"),
            $("#memberLength").val(), $("#memberAngle").val());
        toolState.member_temp_line.set({ 'x2': endPoint.x2, 'y2': endPoint.y2 });
    }
    else if (toolState.cur_tool == "load" && toolState.load_drawing == true) {
        var endPoint = toEndPoint(toolState.load_temp_joint.get("left"),
            toolState.load_temp_joint.get("top"), loadRealLength, $("#loadAngle").val());
        toolState.load_temp_arrow.set({ 'x2': endPoint.x2, 'y2': endPoint.y2 });
    }
    canvas.renderAll();
});

$("#solve-btn").click(function () {
    solve();
});

$("#welcome-modal button").click(function () {
    var canvasWidth = $("#width-input").val();
    if (canvasWidth != "" && !isNaN(canvasWidth) && canvasWidth > 0) {
        initScale(canvasWidth);
        $('#welcome-modal').modal('hide');
    } else {
        $("#width-input").addClass("invalid");
    }
});

$("#btn-export").click(function () {
    $('#export-modal').modal("show");
    $("#txt-export").val(saveToJson());
});

$("#export-modal #btn-export-load").click(function () {
    if (!loadFromJson($("#txt-export").val())) {
        $("#txt-export").addClass("invalid");
    }
});

$("#btn-get-link").click(function () {
    customAlert("Get Link", "Use the url below to share this work with others",
        "<pre class='.pre-scrollable'><code>" + genUrl() + "</code></pre>");
});


window.addEventListener('touchstart', function () {
    touch_device = true;
});




$(document).ready(function () {
    $(function () {
        $('[data-toggle="popover"]').popover()
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $('#alert-modal').modal({ show: false });

    $('#export-modal').modal({ show: false });

    if (!loadFromUrl()) {
        $('#welcome-modal').modal({ show: true, backdrop: "static" });
    }
});
